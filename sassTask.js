var sass = require('node-sass');
var fs = require('fs');

sass.render({
  file: 'main.scss',
  //compressed, nested, expanded, compact
  outputStyle: 'expanded',
  outFile: 'main.css',
  sourceMap: true,
},function(err,result){
    console.log(sass.info);
    console.log(result.css);
    console.log(result.map);
    console.log(result.stats);
    fs.writeFile('main.css',result.css.toString(),function(err){
      if(err) throw err;
      console.log('The file has been saved');
    });
});